# squares_api



## Project Overview

The Squares solution is designed to enable our enterprise consumers to identify squares from coordinates. A user interface allows a user to import a list of points, add and delete a point to an existing list. On top of that the user interface displays a list of squares that our amazing solution identified. 
The squares_api consists of the below end points and methods:
- POST https://localhost:7281/api/point  (posts one point coordinates - a list of two elements that must be integers).
- POST https://localhost:7281/api/points (posts multiple points coordinates - a list of lists that contain two elements per coordinates X and Y).
- GET https://localhost:7281/api/point and GET https://localhost:7281/api/point{id}
- PUT https://localhost:7281/api/point{id}
- DELETE https://localhost:7281/api/point{id}
- GET https://localhost:7281/api/squares (gets number of squares that could be formed from the given coordinates).
- GET https://localhost:7281/api/square_list (returns a list of coordinates for each square that could be formed).

## Prerequisites

- Visual Studio Code
- C# for Visual Studio Code (latest version)
- .NET 6.0 SDK

```
As an example the below commands could be executed in the terminal:
1) httprepl https://localhost:7281/api/point 
2) post -h Content-Type=application/json -c [0,0]
3) get
4) connect https://localhost:7281/api/points
5) post -h Content-Type=application/json -c [[1,0],[0,1],[1,1],[2,2],[2,0],[0,2]] 
6) connect https://localhost:7281/api/point
7) get 
8) connect https://localhost:7281/api/squares
9) get
10) connect https://localhost:7281/api/square_list
11) get
12) connect https://localhost:7281/api/point/{id}
13) put -h Content-Type=application/json -c [3,3]
14) connect https://localhost:7281/api/point
15) get
16) connect https://localhost:7281/api/point/{id}
17) delete
```

