using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using squares_api.Models;

namespace square_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PointController : ControllerBase
    {
        private readonly SquaresContext _context;

        public PointController(SquaresContext context)
        {
            _context = context;
        }

        //  GET: api/point      
        [HttpGet]
        public async Task<ActionResult<Dictionary<string, int[]>>> GetPoints()
        {
            var dictionary = new Dictionary<string, int[]>();

            await foreach (Point point in _context.Points.AsAsyncEnumerable())
            {
                dictionary.Add(point.Id.ToString(), point.Coordinates);
            }

            return dictionary;
        }

        //  GET: api/point/1      
        [HttpGet("{id}")]
        public async Task<ActionResult<int[]>> GetPoint(long id)
        {
            var point = await _context.Points.FindAsync(id);

            if (point == null)
            {
                return NotFound();
            }

            return point.Coordinates;

        }
        // PUT: api/point/1
        
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdatePoint(long id, int[] coordinates)
        {
            var point = await _context.Points.FindAsync(id);

            if (point == null)
            {
                return NotFound();
            }

            point.X = coordinates[0];
            point.Y = coordinates[1];

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException) when (!PointExists(id))
            {
                return NotFound();
            }

            return NoContent();
        }

        //  POST: api/point
        
        [HttpPost]
        public async Task<ActionResult<int[]>> CreatePoint(int[] coordinates)
        {
            if (coordinates.Length != 2)
            {
                return BadRequest();
            }

            var point = new Point(coordinates[0], coordinates[1]);

            _context.Points.Add(point);
            await _context.SaveChangesAsync();

            return CreatedAtAction(             // return http response 201
                nameof(GetPoint),
                new { id = point.Id },
                point.Coordinates);
        }

        //  POST: api/points
        
        [HttpPost]

        [Route("/api/points")]

        public async Task<ActionResult<List<int[]>>> CreatePoints(List<int[]> points)
        {
            foreach (int[] coordinates in points)
                if (coordinates.Length != 2)
                {
                    return BadRequest();
                }

            foreach (int[] coordinates in points)
                _context.Points.Add(new Point(coordinates[0], coordinates[1]));

            await _context.SaveChangesAsync();

            return CreatedAtAction(
                nameof(GetPoints),
                null,
                points);
        }


        //  DELETE: api/point/1
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePoint(long id)
        {
            var point = await _context.Points.FindAsync(id);

            if (point == null)
            {
                return NotFound();
            }

            _context.Points.Remove(point);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool PointExists(long id)
        {
            return _context.Points.Any(e => e.Id == id);
        }
    }
}
