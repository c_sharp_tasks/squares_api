using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using squares_api.Models;

namespace square_api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SquaresController : ControllerBase
    {
        private readonly SquaresContext _context;

        public SquaresController(SquaresContext context)
        {
            _context = context;
        }

        //  GET: api/squares
        [HttpGet]
        public async Task<ActionResult<int>> GetSquares()
        {
            var result = await GetSquaresAsync(5);

            if (result.Value != null)
                return result.Value.Count;

            return result.Result ?? NoContent();
            
        }

        //  GET: api/squares/
        [HttpGet]
        [Route("/api/square_list")]
        public async Task<ActionResult<List<Point[]>>> GetSquarePoints()
        {
            return await GetSquaresAsync(5);

        }

        public async Task<ActionResult<List<Point[]>>> GetSquaresAsync(int timeoutInSeconds)
        {
            var tokenSource = new CancellationTokenSource();
            tokenSource.CancelAfter(timeoutInSeconds * 1000);

            var token = tokenSource.Token;

            var task = new Task<ActionResult<List<Point[]>>>( () =>
            {
                var points = _context.Points.ToArray();
                var squares = Logic.GetSquares(points, token);

                if (token.IsCancellationRequested)
                    return new StatusCodeResult(408);

                var squarePoints = new List<Point[]>();

                foreach (Square square in squares)
                    squarePoints.Add(square.points);

                return squarePoints;
            });

            task.Start();

            return await task;
        }
    }

    internal class Logic {
        internal static HashSet<Square> GetSquares(Point[] input, CancellationToken token)
        {
            HashSet<Square> squares = new HashSet<Square>();

            for (int i = 0; i < input.Length; i++)
            {
                for (int j = i + 1; j < input.Length; j++)
                {
                    //if (i == j)
                    //    continue;

                    int d_ij = SquaredDistance(input[i], input[j]);
                    if (d_ij == 0)
                        continue;

                    for (int k = 0; k < input.Length; k++)
                    {
                        for (int l = k + 1; l < input.Length; l++)
                        {
                            if (token.IsCancellationRequested)
                                return squares;

                            if (k == i || k == j || l == i || l == j/* || k == l*/)
                                continue;

                            int d_kl = SquaredDistance(input[k], input[l]);
                            if (d_kl == d_ij) {
                                if (IsSquare(input[i], input[j], input[k], input[l]))
                                    squares.Add(new Square(input[i], input[j], input[k], input[l]));
                                    System.Console.WriteLine(squares);
                            }
                        }
                    }
                }
            }
            System.Console.WriteLine(squares);
            return squares;
        }

        // A utility function to find square of distance from point 'p' to point 'q'
        static int SquaredDistance(Point p, Point q)
        {
            return (p.X - q.X) * (p.X - q.X) + (p.Y - q.Y) * (p.Y - q.Y);
        }

        // This function returns true if (p1, p2, p3, p4) form a square, otherwise false
        internal static bool IsSquare(Point p1, Point p2, Point p3, Point p4)
        {
            int d2 = SquaredDistance(p1, p2); // from p1 to p2
            int d3 = SquaredDistance(p1, p3); // from p1 to p3
            int d4 = SquaredDistance(p1, p4); // from p1 to p4
        
            if (d2 == 0 || d3 == 0 || d4 == 0)   
                return false;

            // If lengths if (p1, p2) and (p1, p3) are same, then
            // following conditions must met to form a square.
            // 1) Square of length of (p1, p4) is same as twice
            // the square of (p1, p2)
            // 2) Square of length of (p2, p3) is same
            // as twice the square of (p2, p4)
            if (d2 == d3 && 2 * d2 == d4
                && 2 * SquaredDistance(p2, p4) == SquaredDistance(p2, p3)
                && d2 == SquaredDistance(p3, p4))
            {
                return true;
            }
        
            // The below two cases are similar to above case
            if (d3 == d4 && 2 * d3 == d2
                && 2 * SquaredDistance(p3, p2) == SquaredDistance(p3, p4) &&
                d3 == SquaredDistance(p2, p4))
            {
                return true;
            }

            if (d2 == d4 && 2 * d2 == d3
                && 2 * SquaredDistance(p2, p3) == SquaredDistance(p2, p4) &&
                d2 == SquaredDistance(p3, p4))
            {
                return true;
            }

            return false;
        }


    }
}
