using Microsoft.EntityFrameworkCore;
using squares_api.Models;


var builder = WebApplication.CreateBuilder(args);
//builder.WebHost.ConfigureKestrel(opts => { opts.Limits.KeepAliveTimeout = TimeSpan.FromSeconds(5); });

// Add services to the container.

builder.Services.AddControllers();
builder.Services.AddDbContext<SquaresContext>(opt =>
    opt.UseInMemoryDatabase("Points"));


var app = builder.Build();

// Configure the HTTP request pipeline.

if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();

}


app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();