using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace squares_api.Models
{
    public class SquaresContext : DbContext
    {
        public SquaresContext(DbContextOptions<SquaresContext> options)
            : base(options)
        {
        }

        public DbSet<Point> Points { get; set; } = null!;
    }
}
