namespace squares_api.Models
{
    public class Square : IEquatable<Square> 
    {
        public Square(Point p1, Point p2, Point p3, Point p4)
        {
            Point[] points = new Point[4] { p1, p2, p3, p4 };
 
            this.points = points.OrderBy(p => p.X).ThenBy(p => p.Y).ToArray();
        }

        public override int GetHashCode()
        {
            unchecked // Overflow is fine, just wrap
            {
                int hash = 17;

                for (int i = 0; i < points.Length; ++i)
                    hash = hash * 23 + points[i].GetHashCode();

                return hash;
            }
        }

        public override bool Equals(object? obj)
        {
            return obj is Square && Equals((Square) obj);
        }

        public bool Equals(Square? square) // only unique remain
        {
            if (square == null)
                return false;

            for (int i = 0; i < points.Length; ++i) {
                if (!square.points[i].Equals(points[i]))
                    return false;
            }

            return true;
        }

        public Point[] points;
    };
}
