using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace squares_api.Models
{
    public class Point : IEquatable<Point>
    {
        public long Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

        [NotMapped]
        public int[] Coordinates {
            get {
                return new int[2] { X, Y };
            }
        }

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override int GetHashCode()
        {
            return (17 + X.GetHashCode()) * 23 + Y.GetHashCode();
        }

        public override bool Equals(object? obj)
        {
            return obj is Point && Equals((Point) obj);
        }

        public bool Equals(Point? p)
        {
            if (p == null)
                return false;
 
            return X == p.X && Y == p.Y;
        }
    };
}